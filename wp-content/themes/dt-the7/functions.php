<?php
/**
 * Vogue theme.
 *
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Initialize theme.
 *
 * @since 1.0.0
 */
require( trailingslashit( get_template_directory() ) . 'inc/init.php' );

function echostatus($statusnoc){
	$statuswithc = ucfirst($statusnoc);
	return $statuswithc;
}

add_filter( 'gettext', 'replace_text_1' );

function replace_text_1( $text ){
    if( $text === 'by' ) {
        $text = 'door';
    }
    return $text;

}

add_filter( 'gettext', 'replace_text_2' );

function replace_text_2( $text ){
    if( $text === 'Submit your review' ) {
        $text = 'In ons gastenboek kan je berichten plaatsen . Deze zijn door iedereen te bekijken.';
    }
    return $text;

}

add_filter( 'gettext', 'replace_text_3' );

function replace_text_3( $text ){
    if( $text === 'Click here to hide form' ) {
        $text = 'Nieuw bericht plaatsen in het AOM-gastenboek';
    }
    return $text;

}

add_filter( 'gettext', 'replace_text_4' );

function replace_text_4( $text ){
    if( $text === 'Rating' ) {
        $text = 'Beoordeling';
    }
    return $text;

}

add_filter( 'gettext', 'replace_text_5' );

function replace_text_5( $text ){
    if( $text === 'Name' ) {
        $text = 'Naam';
    }
    return $text;

}
