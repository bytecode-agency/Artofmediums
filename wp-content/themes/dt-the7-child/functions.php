<?php

//Text verwijderen
add_filter( 'gettext', 'replace_preview_resume_text' );
 
function replace_preview_resume_text( $text ){
 
    if( $text === 'Dream-Theme — truly premium WordPress themes' ) {
        $text = '';
    }
 
    return $text;
}

// Shortcodes
add_shortcode('carouselagent', 'carouselagent');
add_shortcode('bestel', 'bestel');
add_shortcode('credits', 'credits');

// Functions 
function carouselagent(){ 
global $wpdb;
$agent_rs = $wpdb->get_results( "SELECT * FROM agents d inner join agents_status cd on d.status = cd.status order by cd.ID, boxnummer",ARRAY_A );
	if(!empty($agent_rs)){
		foreach($agent_rs as $row_agents){?>		
		<div class="col-1-4">
				<div class="mediumbox">
					<img src="/script/pf/<?php echo $row_agents['Afbeelding'] ?>.png" class="afbeeldingmedium">
					<div class="naampaars"><?php echo $row_agents['naam']; ?></div>
					<div class="profiel"><?php echo $row_agents['Profiel']; ?></div>
					<br>
					<img alt="<?php echo $row_agents['status']; ?>" src="/script/image/<?php echo ucfirst($row_agents['status']) ?>.gif" style="float: left; margin-left: 10px; padding-right: 5px; padding-top: 5px;">
					<div class="status"><?php echo ucfirst($row_agents['statusweb']) ?></div>
					<div class="leesmeer"><a href="/medium/<?php echo $row_agents['Afbeelding']; ?>"><b><u>Lees meer</b></u></a></div>
					<br>
					<div class="nummer">Boxnummer: <strong><?php echo $row_agents['boxnummer']; ?></strong></div>
				</div>
			</div><?php	
		}
	}
}

function bestel(){ include 'script/bestel.php'; }
function credits(){ include 'script/credits.php'; }

// Cronjob
add_action('carouselagent', 'carouselagent');

add_action('wp_head','load_carousels');
function load_carousels() {
	?>
	<script>
	$(document).ready(function(){
	$.ajax({
    type: "GET",
    url: "http://www.artofmediums.be/script/carouselagent_update.php",
	})
	});
	</script>
	<?php
}

?>