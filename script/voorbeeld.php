<?php
$date1 = round(microtime(true)*1000);

// by Massxess (C) 2013
// <operatoins@massxess.nl>
// Id: carousel example script , v 1.2 2013/11/11 14:21:47 CET
// Copyright (c) 2013 Massxess.
// All rights reserved.
// 		
// -	Er moet een database aangemaakt moet worden (carouselagent) en een Table (agents)
// -	Bepaalde gegevens moeten worden gevuld (boxnummer, extensionId, extId etc)
// -	Grbruik dit script dus alleen.
// -	De andere XML-RPC functies hoeven niet te worden ge�mplementeerd.
// -	Verder moet je een script schrijven dat de database uitleest
// -	Je moet het script als een cronjob moet runnen.
// 
// 		
/********************************************************************************************************************************************/
/*                                                                            AGENTS TABLE STRUCTURE                                        */
/*                                                                                                                                          */
/*       Column                  Type            Null     Default                   Comments                                                */
/*       uniqueDBId              int(11)          No                                                                                        */
/*       boxnummer               varchar(20)      No                                                                                        */
/*       naam                    varchar(25)      No                                                                                        */
/*       extensionId             varchar(6)       No                                                                                        */
/*       extId                   varchar(6)       No                                                                                        */
/*       status                  varchar(20)      No                                                                                        */
/*       Afbeelding              varchar(250)     No                                                                                        */
/*       Profiel                 varchar(250)     No                                                                                        */
/*       .-Indexes----------------------------------------------------------------------------------------------------------------------.   */
/*       |        Keyname      Type   Unique Packed   Veld            Column         Cardinality     Collation         Null     Comment |   */
/*       |        PRIMARY      BTREE  Yes     No      uniqueDBId    2                 A                                          No     |   */
/*       |______________________________________________________________________________________________________________________________|   */
/*                                                                                                                                          */
/********************************************************************************************************************************************/


//$username="root";
//$password="usbw";
//$server="localhost";
//$db="carouselagent";

error_reporting(E_ALL & ~E_NOTICE);







function DBOpen($server, $username, $password, $db)
{
         //Make SQL connection and select db
         $link = mysql_connect($server, $username, $password);
         if(!$link)
                   writeLog("ERROR! MySQL " . mysql_errno() . ": " . mysql_error());
         else
         {
                   $db_selected = mysql_select_db($db, $link);
                   if(!$db_selected)
                   {
                            writeLog("ERROR! MySQL " . mysql_errno($link) . ": " . mysql_error($link));
                            //Unable to select DB, so closing db connecting.
                            if(DBClose($link))
                                      $link=false;
                            else
                                      writeLog("ERROR! MySQL Something went terrible wrong on connection closing");
                   }
         }
         return $link;
}
function DBClose($conn)
{
         //Close the SQL connection.
         if($conn)
                   return mysql_close($conn);
         else
                   return true;
}

//just write received info to a log file.
function writeLog($logstring)
{
         //define logfile
         $myFile = "carousel_log.txt";
         //write log
         $fh = fopen($myFile, 'a') or die("can't open file");
         fwrite($fh,"-------new LOG on " . get_today() . ": " . $logstring . " -------\r\n");
         //close log
         fclose($fh);
}

// give a nice date time format for logging
function get_today(){
         $today = getdate();
         return $today["mday"] . "-" . $today["mon"]  . "-" . $today["year"] . " " . $today["hours"] . ":" . $today["minutes"] . ":" . $today["seconds"];
}        






?>

<?php
$con=mysqli_connect($server, $username, $password, $db);

$date2 = round(microtime(true)*1000);
//echo $date2;
//echo "<br>";
$date3 = ($date2 - $date1);
echo "Update ". $date3 ."ms."."<br>";

// Check connection
if (mysqli_connect_errno()) {
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
?>

<style>
	/* grid */
.row {
  margin: 0 -10px;
  margin-bottom: 20px;
}
.row:last-child {
  margin-bottom: 0;
}
[class*="col-"] {
  padding: 10px;
}
  .col-1-4 {
    float: left;
    width: 23%;
  }

@media all and ( max-width: 1150px ) {
  .col-1-4{
    width: 45%;
    float: left;
  }
  .col-1-4:nth-child(2n+1) {
    clear: left;
  }
}

@media all and ( max-width: 600px ) {
  .col-1-4{
    width: 100%;
  }
  .col-1-4:nth-child(4n+1) {
    clear: none;
  }
}

	.mediumbox {
		border-style: solid; 
		border-width: 2px; 
		border-radius: 15px; 
		border-color: #8224e3;
	}
	
	.afbeeldingmedium {
		border-top-left-radius: 13px;
		border-top-right-radius: 13px; 
		display: block;
		width: 80%;
	}
	
	.profiel {
		text-align: left;
		height: 100px;
		color: #8224e3;							
		margin-left: 5px;
		overflow: hidden;
	}
	
	.naampaars {
		text-align: center;
		font-size: 24pt;
		font-weight: 500;
		color: #8224e3;
		padding-top: 20px;
		padding-bottom: 20px;
		border-top: 2px solid #8224e3;
		border-bottom: 2px solid #8224e3;
	}

	.status {
		text-align: left;
		font-size: larger;
		color: #8224e3;
		margin-left: 5px;
		margin-bottom: 5px;
		float: left;
	}
	
	.statusgif {
		float: left;
		margin-left: 5px; 
		padding-right: 5px; 
		padding-top: 5px;
	}
	
	.nummer {
		text-align: center;
		font-weight: 200;
		color: white;
		background-color: #8224e3;
		border-bottom-left-radius: 12px;
		border-bottom-right-radius: 12px;
		clear: both;
	}
	
	.leesmeer {
		text-align: right;
		color: #8224e3;
		margin-right: 5px;
		margin-bottom: 5px;
		float: right;
	}	
	
	/* unvisited link */
	a:link {
		text-decoration: none;
		color: #8224e3;
	}			    
	/* visited link */
	a:visited {
		text-decoration: none;
		color: #8224e3;
	}	
	
/*		color: #007700; 	*/			/* Green  */
/*		color: #6698FF; */				/* Blauw  */
/*		color: #800080; */				/* Paars  */
/*		color: #ff0000; */				/* Red    */	
/*		font-size: larger;*/
	
</style>

<?php $result = mysqli_query($con,"SELECT * FROM agents d inner join agents_status cd on d.status = cd.status order by cd.ID, boxnummer"); ?>
	<?php while($row = mysqli_fetch_array($result)) { ?>
		<div class="col-1-4">
			<div class="mediumbox">
				<img src="/script/pf/<?php echo $row['Afbeelding'] ?>.png" class="afbeeldingmedium">
				<div class="naampaars"><?php echo $row['naam']; ?></div>
				<div class="profiel"><?php echo $row['Profiel']; ?></div>
				<br>
				<img alt="<?php echo $row_agents['status']; ?>" src="/script/image/<?php echo $row['status'] ?>.gif" class="statusgif">
				<div class="status"><?php echo ucfirst($row['statusweb']) ?></div>
				<div class="leesmeer"><a href="/medium/<?php echo $row['Afbeelding']; ?>"><b><u>Lees meer</b></u></a></div>
				<br>
				<div class="nummer">Boxnummer: <strong><?php echo $row['boxnummer']; ?></strong></div>
			</div>
		</div>
	<?php } ?>
	
<?php mysqli_close($con); ?>



