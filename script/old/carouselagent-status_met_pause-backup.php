<!DOCTYPE html>

<?php
$date1 = round(microtime(true)*1000);

// by Massxess (C) 2013 || Edited by Nooijen Solutions, 2017
// <operatoins@massxess.nl> <info@nooijensolutions.nl>
// Id: carousel example script , v 1.2 2013/11/11 14:21:47 CET
// Copyright (c) 2013 Massxess and 2017 Nooijen Solutions
// All rights reserved.
// 		
// -	Er moet een database aangemaakt moet worden (carouselagent) en een Table (agents)
// -	Bepaalde gegevens moeten worden gevuld (boxnummer, extensionId, extId etc)
// -	Grbruik dit script dus alleen.
// -	De andere XML-RPC functies hoeven niet te worden ge�mplementeerd.
// -	Verder moet je een script schrijven dat de database uitleest
// -	Je moet het script als een cronjob moet runnen.
// 
// 		
/********************************************************************************************************************************************/
/*                                                                            AGENTS TABLE STRUCTURE                                        */
/*                                                                                                                                          */
/*       Column                  Type            Null     Default                   Comments                                                */
/*       uniqueDBId              int(11)          No                                                                                        */
/*       boxnummer               varchar(20)      No                                                                                        */
/*       naam                    varchar(25)      No                                                                                        */
/*       extensionId             varchar(6)       No                                                                                        */
/*       extId                   varchar(6)       No                                                                                        */
/*       status                  varchar(20)      No                                                                                        */
/*       Afbeelding              varchar(250)     No                                                                                        */
/*       Profiel                 varchar(250)     No                                                                                        */
/*       .-Indexes----------------------------------------------------------------------------------------------------------------------.   */
/*       |        Keyname      Type   Unique Packed   Veld            Column         Cardinality     Collation         Null     Comment |   */
/*       |        PRIMARY      BTREE  Yes     No      uniqueDBId    2                 A                                          No     |   */
/*       |______________________________________________________________________________________________________________________________|   */
/*                                                                                                                                          */
/********************************************************************************************************************************************/


$username="nooijensols_aom";
$password="nooijen123";
$server="localhost";
$db="nooijensols_aom";

error_reporting(E_ALL & ~E_NOTICE);
//include the XMLRPC module Change the pad to your XMLRPC directory
include("script/lib/xmlrpc.inc");
//accountcode
$accountcode = "53560"; //please, change with your accountcode
//supid Massxess
$supid = "1"; //please, don't change this value
//manid Massxess
$manid = "2"; //please, don't change this value
//cust id Massxess customer
$custid = "5147"; //please, change with your customer id, ask Massxess when unknown
// Max aantal skills (optioneel)
$max = "20";

function DBOpen($server, $username, $password, $db)
{
         //Make SQL connection and select db
         $link = mysql_connect($server, $username, $password);
         if(!$link)
                   writeLog("ERROR! MySQL " . mysql_errno() . ": " . mysql_error());
         else
         {
                   $db_selected = mysql_select_db($db, $link);
                   if(!$db_selected)
                   {
                            writeLog("ERROR! MySQL " . mysql_errno($link) . ": " . mysql_error($link));
                            //Unable to select DB, so closing db connecting.
                            if(DBClose($link))
                                      $link=false;
                            else
                                      writeLog("ERROR! MySQL Something went terrible wrong on connection closing");
                   }
         }
         return $link;
}
function DBClose($conn)
{
         //Close the SQL connection.
         if($conn)
                   return mysql_close($conn);
         else
                   return true;
}

function getAllAgents($conn)
{

         //check if the SQL connection is still open
         if(!$conn)
         {
                   writeLog("ERROR! No database connection");
                   $agent = NULL;
         }
         else
         {
                   //do a SQL query to get all agents. Create array with agent object.
                   $result = mysql_query('SELECT status, boxnummer, extensionId, extId, naam, uniqueDBId FROM agents', $conn);
                   while ($row = mysql_fetch_assoc($result)) {
                            $agent[$row['boxnummer']] = new OAgent($row['status'], $row['boxnummer'], $row['extensionId'],$row['extId'], $row['naam'], $row['uniqueDBId']);
                   }
                   mysql_free_result($result);
         }
         return $agent;
}

function updateAgentStatus($conn, $agents)
{
         //check if the SQL connection is still open
         if(!$conn)
         {
                   writeLog("ERROR! No database connection");
                   return false;
         }
         else
         {
                   //call the function setStatusAvailAgents. This function will return the array of all agents and update the status for Online agents
                   $agents = setStatusAvailAgents($agents);
                   //loop trough array and update the database if agent status has changed
                   foreach($agents as $agent)
                   {
	                   if($agent->status == "Offline")
  	                    	$agent->setStatus(getLoginStatus($agent->extensionId));

                 		elseif($agent->status == "Pause")
	                 		$agent->setStatus(getLoginStatus($agent->extensionId));

                 		elseif($agent->status == "Afhandelen")
	                 		$agent->setStatus(getLoginStatus($agent->extensionId));
             			
                        if($agent->updateStatus())
                             $result = mysql_query("update agents set Status='".$agent->status."' WHERE uniqueDBId='".$agent->dbId."'", $conn);
                   }
         }
         return true;
}                                    

function setStatusAvailAgents($agents)
{
         global $accountcode, $supid, $manid, $custid, $max;
         $v=new xmlrpcval(array("supId" =>
                        new xmlrpcval($supid, 'string'),
                        "manId" =>
                        new xmlrpcval($manid , 'string'),
                        "custId" =>
                        new xmlrpcval($custid , 'string')),
                    "struct");
    //XML query 
    $msg = new xmlrpcmsg("remmerden.getItems", $v);
    //add XML structure
    $msg->addParam($v);
    //call client XMLRPC function
    $client = new xmlrpc_client("http://www.ivrmanager.nl/xml/remmerden/xml.asp");
    //set debug option (0=none, 1=only received data,2=full (send and receive))
    $client->setDebug(0);
    //do the XML call to ivrmanager
    $result=$client->send($msg);
    //check if XML call has errors
    if(!$result->faultCode())
         {
                   $response = $result->value();
                   for($x=0;$x<$response->structmem("itemCount")->scalarval();$x++)
                   {
                            $boxnummer = $response->structmem("item$x")->scalarval();
                            if(array_key_exists($boxnummer, $agents))
                                      $agents[$boxnummer]->setStatus(getCallingStatus($manid, $custid, $supid, $agents[$boxnummer]->extId));
                            else
                                      writeLog("WARNING! boxnummer ". $boxnummer . " not found in agentslist");
                   }
         }
         else
         {
                   writeLog("ERROR! Failed to receive Online list of skills");
         }
         return $agents;
}

//Only needed if you want to know the Offline status (Not used in this example)
function getLoginStatus($extensionid)
{
    global $accountcode;
	     //create XML structure for login status agent
    $v=new xmlrpcval(array("accountCode" =>
                            new xmlrpcval($accountcode, 'string'),
                                        "extensionId" =>
                            new xmlrpcval($extensionid , 'string')),
                                        "struct");
    //XML query 
    $msg = new xmlrpcmsg("ivr.getAgentAvail", $v);
    //add XML structure
    $msg->addParam($v);
    //call client XMLRPC function
    $client = new xmlrpc_client("http://www.ivrmanager.nl/xml/agent/xml.asp");
    //set debug option (0=none, 1=only received data,2=full (send and receive)
    $client->setDebug(0);
    //do the XML call to ivrmanager
    $result=$client->send($msg);
    //check if XML call has errors
    if(!$result->faultCode())
    {
        $agent=$result->value();                
        $agent_avail=$agent->structmem("available");
        $agent_reason=$agent->structmem("availReason");
        if ($agent_avail->scalarval() == "1")
                   {
                            $status = "Online";
                   }
                   else
                   {
                            switch (IsSet($agent_reason) ? trim($agent_reason->scalarval()) : "Offline")
            {
                case "pause":
                    $status = "Pause";
                    break;
                case "handling":
                    $status = "Afhandelen";
                    break;
                default:
                    //If not both of the above values are true, the agent is Offline!
                    $status = "Offline";
                    break;   
            }
                   }
         }
         else
         {
                   writeLog("WARNING! Receive agent login status failed for extension $extensionid");
                   $status = "Niet bekend";
         }
         return $status;
}

function getCallingStatus($manid, $custid, $supid, $extid)
{
         $v=new xmlrpcval(array("supId" =>
                                                                           new xmlrpcval($supid, 'string'),
                            "manId" =>
                                                                           new xmlrpcval($manid , 'string'),
                            "custId" =>
                                                                           new xmlrpcval($custid , 'string'),
                            "extId" =>
                                                                           new xmlrpcval($extid , 'string')),
                            "struct");

    //var_dump($result->value());
    $msg = new xmlrpcmsg("general.getExtensionStatusExt", $v);
    //add XML structure
    $msg->addParam($v); 
    //call client XMLRPC function
    $client = new xmlrpc_client("http://www.ivrmanager.nl/xml/general/xml.asp");
    //set debug option (0=none, 1=only received data,2=full (send and receive)
    $client->setDebug(0);
    //do the XML call to ivrmanager
    $result=&$client->send($msg);

    if(!$result->faultCode())
    {
        $agent=$result->value();                
        $agent_status=$agent->structmem("result");
        switch (trim($agent_status->scalarval()))
        {
        case "Idle":
					$status = "online";
				break;
				case "wrapup":			// Na werktijd
					$status = "busy";
				break;				// Deze wordt gebeld
        case "called":
					$status = "busy";
				break;
        case "calling":
					$status = "busy";
				break;
        }
    }
         else
         {
                   writeLog("WARNING! Receive agent Status failed for extId $extid");
                   $status = "unknown";
         }        
         return $status;
}

//just write received info to a log file.
function writeLog($logstring)
{
         //define logfile
         $myFile = "carousel_log.txt";
         //write log
         $fh = fopen($myFile, 'a') or die("can't open file");
         fwrite($fh,"-------new LOG on " . get_today() . ": " . $logstring . " -------\r\n");
         //close log
         fclose($fh);
}

// give a nice date time format for logging
function get_today(){
         $today = getdate();
         return $today["mday"] . "-" . $today["mon"]  . "-" . $today["year"] . " " . $today["hours"] . ":" . $today["minutes"] . ":" . $today["seconds"];
}        

//Agent object
class OAgent
{
         public $status;
         private $oude_status;
         public $boxnummer;
         public $extensionId;
         public $extId;
         public $naam;
         public $dbId;
         private $changed = false;

         function OAgent($status, $boxnummer, $extensionId, $extId, $naam, $dbId)
         {
                   $this->status = $status;
                   $this->boxnummer = $boxnummer;
                   $this->extensionId = $extensionId;
                   $this->extId = $extId;
                   $this->naam = $naam;
                   $this->dbId = $dbId;
         }

         function setStatus($status)
         {
                            $this->oude_status = $this->status;
                            $this->status = $status;
                            $this->changed = true;
         }

         function updateStatus()
         {
                   if($this->status != "Offline" && !$this->changed) 
                   {
                            $this->status = "Offline";
                            return true;
                   }
                   elseif($this->changed && $this->status!=$this->oude_status)
                            return true;
                   else
                            return false;
         }

}

//Open Database connection
$conn = DBOpen($server, $username, $password, $db);
if($conn)
{
         //Get all the agents from db and their status
         $agentList = getAllAgents($conn);
         // retrieve new status and update db
         if(!is_null($agentList))
                   updateAgentStatus($conn, $agentList);
         //All done! Close Database connection
         if(!DBClose($conn))
                   writeLog("ERROR! MySQL Something went terrible wrong on connection closing");
}
/*
?>

<?php echo $status; ?>

<?php
*/
$con=mysqli_connect($server, $username, $password, $db);

// Check connection
if (mysqli_connect_errno()) {
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//$result = mysqli_query($con,"SELECT * FROM agents ");
$result = mysqli_query($con,"SELECT * FROM agents where status <> 'Offline'");

//echo $date1;
//echo "<br>";
$date2 = round(microtime(true)*1000);
//echo $date2;
//echo "<br>";
$date3 = ($date2 - $date1);
//echo $date3 ."ms!";
//echo "<br>";
//echo "<br>";
echo "<table vertical-align=\"top\" border=\"1\";>";

while($row = mysqli_fetch_array($result)) {
echo "<tr border=1>";
//<td WIDTH='350';vertical-align='top'; align='right'>

echo "<td width=\"200\" valign='top'>";
	echo "Boxnummer: " .  $row['boxnummer'];
	echo "<br>"; 
	echo $row['naam'];
	echo "<br>"; 
	echo $row['status'];
echo "</td>";

echo "<td border=1 valign='top'>";
//echo '<td><img src="' . $row['Afbeelding']. '" border=2 ></td>'; 
//echo '<img src="' . $row['Afbeelding']. '" border=2 >';
//echo "<br>";
	echo '<img src="script/image/' . $row['status']. '.gif" border=2 >';
	echo "<br>";
	echo $row['status'];
echo "</td>";

echo "<td width=\"250\" border=1>";
	echo $row['Profiel'];
	echo "</td>";
	echo "</tr>";
}
//echo '<img src="http://xxxxx.nl/wp-content/uploads/2014/09/J2.jpg" border=1>';
//echo "<br>";   echo "<br>";
//echo "<br>";   echo "<br>";   }
echo "</table>";

$result = mysqli_query($con,"SELECT * FROM agents where status = 'Offline'");
echo "<br>";
echo "<br>";
echo "<table vertical-align=\"top\" border=\"1\";>";

while($row = mysqli_fetch_array($result)) {
echo "<tr border=1>";
//<td WIDTH='350';vertical-align='top'; align='right'>

echo "<td width=\"200\" valign='top'>";
	echo "Boxnummer: " .  $row['boxnummer'];
	echo "<br>"; 
	echo $row['naam'];
	echo "<br>"; 
	//echo $row['status'];
echo "</td>";

echo "<td border=1 valign='top'>";
//echo '<td><img src="' . $row['Afbeelding']. '" border=2 style="width: 100px;"></td>'; 
//echo '<img src="' . $row['Afbeelding']. '" border=2 >';
//echo "<br>";
	echo '<img src="script/image/' . $row['status']. '.gif" border=2 >';
//	echo "<br>";
	echo $row['status'];
	echo '<img src="' . $row['Afbeelding']. '" border=2 style="max-width: 200px; max-height: 120px;">'; 
echo "</td>";

echo "<td width=\"250\" border=1>";
	echo $row['Profiel'];
	echo "</td>";
	echo "</tr>";
}
//echo '<img src="http://xxxxx.nl/wp-content/uploads/2014/09/J2.jpg" border=1>';
//echo "<br>";   echo "<br>";
//echo "<br>";   echo "<br>";   }
echo "</table>";

mysqli_close($con);
?>